SARTopo Custom Markers
======================

Purpose
-------

There were several SAR related icons that were not included in SARTopo by
default. The initial set in this package included things like:

-   ICP: Incident Command Post

-   IPP: Initial Planning Point

-   LKP: Last Known Point

-   PLS: Place Last Seen

-   RP: Reporting Party

-   UAS: Unmanned Aircraft Launch Point

![image](SVG/SAR/ICP.svg){width=10%}![image](SVG/SAR/IPP.svg){width=10%}![image](SVG/SAR/LKP.svg){width=10%}
![image](SVG/SAR/PLS.svg){width=10%}![image](SVG/SAR/RP.svg){width=10%}![image](SVG/SAR/UAS.svg){width=10%}


Uploading Icons to your SARTopo Account
---------------------------------------

-   Log in to SARTopo

-   Click on **Your Data** at the top right corner

-   Click on the **Plus** icon at the top right corner (near the magnifying
    glass)  
    

![image](Resources/Images/Add_Item.png){width=25%}

-   Click on **Icon**

-   In the **Add Icon **window:

    -   Give your icon a name, such as PLS

    -   Choose if you want to save it in your own account, or if you want to
        save it to your Team Account

    -   Select a **Folder** to place the icons

    -   Upload your Icon file. The SVG format allows the icons to scale and
        maintain the highest quality

    -   I leave the size at 24px as a default

    -   **Optional:** If you want the icons to be colorable on your map, turn
        this option on, otherwise leave it off and the icon will remain black
        and white.

    -   **Optional: **For most of these icons, rotating them is probably not
        necessary, so you can leave this off, but other icons you might upload
        might benefit from being rotatable, so you can enable this option as
        needed.

![image](Resources/Images/Add_Icon.png){width=50%}

Editing Icons or Adding Additional Icons
----------------------------------------

The Adobe Illustrator versions of the original icons will be added to this
project upon request.
